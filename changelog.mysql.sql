-- liquibase formatted sql

-- changeset root:1615584956471-1
CREATE TABLE MyTable (MyEuroColumn VARCHAR(50) NULL);

-- changeset root:1615584956471-2
CREATE TABLE department (id INT NOT NULL, name VARCHAR(50) NOT NULL, active BIT(1) DEFAULT 1 NULL, CONSTRAINT PK_DEPARTMENT PRIMARY KEY (id));

-- changeset root:1615584956471-3
CREATE TABLE person (id INT AUTO_INCREMENT NOT NULL, firstname VARCHAR(50) NULL, lastname VARCHAR(50) NOT NULL, state CHAR(2) NULL, username VARCHAR(8) NULL, CONSTRAINT PK_PERSON PRIMARY KEY (id));

-- changeset root:1615584956471-4
CREATE TABLE state (id CHAR(2) NOT NULL, CONSTRAINT PK_STATE PRIMARY KEY (id));

-- changeset root:1615584956471-5
CREATE VIEW person_view AS select `DEV`.`person`.`id` AS `id`,`DEV`.`person`.`firstname` AS `firstname`,`DEV`.`person`.`lastname` AS `lastname`,`DEV`.`person`.`state` AS `state`,`DEV`.`person`.`username` AS `username` from `DEV`.`person`;

-- changeset root:1615584956471-6 splitStatements:false
CREATE FUNCTION calcProfit ( cost FLOAT( 12 ), price FLOAT( 12 ) )
RETURNS DECIMAL(9,2) DETERMINISTIC

BEGIN
 DECLARE profit DECIMAL(9,2);
 SET profit = price-cost;
 RETURN profit;
END;

-- changeset root:1615584956471-7 splitStatements:false
CREATE PROCEDURE HelloWorldProcedure (  )

BEGIN
    select 'HELLO WORLD' as ' ';
END;


--changeset SteveZ:45556-createtableactor
CREATE TABLE actor (
 name varchar(50) NOT NULL
);
--rollback drop table actor;

--changeset SteveZ:45679-actor_name_idx_IS_NOT_NULL
CREATE INDEX actor_name_idx ON actor (name) USING btree WHERE name IS NOT NULL;
--rollback drop INDEX actor_name_idx
